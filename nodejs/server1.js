// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var db = require("./mysql_local");
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();               

// accessed at GET http://localhost:port/getUsers

router.route('/logIn').post(function(req, res){

	var mysql_conn = db.connection();
	var query = "select * from users where password = MD5('"+req.body.password+"') and username = '"+req.body.username+"';"
	console.log(query);

	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log(rows.length);
	  
	  if(rows.length == 1){
	  	req.session.user = rows[0];
	  	res.json({ message: "SUCCESS" });
	  }
	  else{
	  	res.json({ message: "ERROR" });
	  }

	});


	mysql_conn.end(); 
});


router.route('/whoRU').get(function(req, res){
	res.json("El usuario es: " + req.session.user);
});

router.route('/getUsers').get(function(req, res){
	var mysql_conn = db.connection();
	console.log("get Users GET");
	var region = req.params.region, date = req.params.date;
	var query = "select * from users where date > '" + date + 
		"' and region = '" + region + "'";
	mysql_conn.connect();	
	mysql_conn.query(query, function(err, results) {
	  if (err) throw err;
		res.json(results);
	});

	mysql_conn.end();

});

// accessed at POST http://localhost:port/registerUser
router.route('/registerUser').post(function(req, res) {
	var mysql_conn = db.connection();
	var query = "insert into users(username, first_name, last_name, email, date_created, region, password) values('"+
		req.body.username+"', '"+req.body.firstname+"', '"+req.body.lastname+"', '"+req.body.email+"', NOW(), '"+req.body.region+
		"', MD5('"+req.body.password+"'));";
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log('Row inserted...');
	});

	mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at POST http://localhost:port/sendMessage
router.route('/sendMessage').post(function(req, res) {
	var mysql_conn = db.connection();
	var query = "insert into messages(message_text, message_read, user_from, user_to, date_created) values('"+ 
		req.body.message+"', 0, '"+req.body.user_from+"', '"+req.body.user_to+"', NOW());";
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
	  if (err) throw err;
	  console.log('Row inserted...');
	});

	mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at GET http://localhost:port/getMessages
router.route('/getMessage').get(function(req, res){
	console.log("GET getMessage");
	res.json("GET getMessage");
	var query = "select * from users where user = '' and read = 0";

});


router.route('/deleteUser').post(function(req, res){
	var mysql_conn = db.connection();
	var query = "delete from users where username = '"+req.body.username+"';"

	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
		if (err) throw err;
		console.log('Table USERS reseted...');
	});
	mysql_conn.end();
	res.json({message: 'Statement executed: ' + query, response: "SUCCESS"});
});


router.route('deleteAllUsers').post(function(req, res) {
	var mysql_conn = db.connection();
	var query = "Truncate table USERS";

	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
		if (err) throw err;
		console.log('Table USERS reseted... ');
	});
	mysql_conn.end();
	res.json({message: 'Statemente executed: ' +query, response: "SUCCESS"});
});



app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Starting server on port ' + port);