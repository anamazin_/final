// BASE SETUP
// =============================================================================

// call the packages we need
var mysql_conn = require("./mysql_local").connection(); //pide un archivo llamado mysql_local
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser'); //se estan pidiendo las librerias que se van a usar (lineas 8, 9, 10)
var sessions = require("client-sessions");


//allow cross side domain requests -- desactivar la seguridad del servidor para poder hacer llamadas al servidor 
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);
//cookie
app.use(sessions({
  cookieName: 'session', // cookie name dictates the key name added to the request object 
  secret: 'blargadeeblargblarg', // should be a large unguessable string 
  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms 
  activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds 
}));


//todo lo que esta en las lineas de arriba SIEMPRE va a ser igual y tiene que estar... no hay de otra

var port = process.env.PORT || 8080;        // set our port -- puerto en el que se va a correr la aplicación

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();  //define donde se van a tene los get, delete, put y post            

// accessed at POST http://localhost:8080/registerUser
router.route('/logIn').post(function(recibe, envia){
	recibe.session.user = recibe.body.user;
	envia.json("Hello there");
});

router.route('/who').get(function(recibe, envia){
	envia.json("User: " + recibe.session.user);
});

router.route('/getUsers').get(function(recibe, envia){
	console.log("get Users GET");
	envia.json("GET getUsers");
	var region = recibe.params.region, fecha = recibe.params.date;
	var query = "select * from users where date > '" + date + " ' and region = '" ;
	connection.query(query, function(err, results) {
		if (err) throw err;
		envia.json(results);
	});
});

router.route('/getMessage').get(function(recibe, envia){
	console.log("get Message GET");
	envia.json("GET getMessage");
	var query = "select * from users where user = '' and read = 0" ;
});

router.route('/registerUser').post(function(recibe, envia){
	var mysql_conn = db.connection();
	var query = "insert into users(username, first_name, last_name, email, date_created, region, password) values('"+
		recibe.body.username+"', '"+recibe.body.firstname+"', '"+recibe.body.lastname+"', '"+recibe.body.email+"', NOW(), '"+recibe.body.region+
		"', MD5('"+recibe.body.password+"'));";
	
});

router.route('/sendMessage').post(function(recibe, envia){
	console.log("post sendMessage POST");
	envia.json("POST sendMessage");
	var query = "insert into messages(message, read, user_from, user_to, date_sent)" + "values('" + recibe.body.message +"', '" + recibe.body.read +"', '" + recibe.body.user_from +"', '" + recibe.body.user_to +"', NOW(), 0)";
	
	envia.json(query);
	
	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields){});
	mysql_conn.end();
});


router.route('/delete').delete(function(recibe, envia){
	var mysql_conn = db.connection();
	var query = "truncate table users";

 	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {
		if(err) throw err 
			console.log(dfghj);		
		});
	mysql_conn.end();
	envia.json({message: 'Statement executed: ' + query, response: "SUCCESS"});
	
});



app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Starting server on port 8080');